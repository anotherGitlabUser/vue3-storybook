module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/vue3-essential', '@vue/airbnb'],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    complexity: ['error', 6],
    'max-statements-per-line': [
      'error', {
        max: 2,
      },
    ],
    'max-nested-callbacks': ['error', 4],
    'max-depth': [
      'error', {
        max: 2,
      },
    ],
    'object-property-newline': ['error', { allowAllPropertiesOnSameLine: false }],
    'max-len': ['error', { code: 120 }],
    'vue/padding-line-between-blocks': ['error', 'always'],
    'array-element-newline': [
      'error', {
        multiline: false,
        minItems: 3,
      },
    ],
    'array-bracket-newline': [
      'error', {
        multiline: true,
        minItems: 3,
      },
    ],
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/unit/**/*.spec.{j,t}s?(x)'],
      env: {
        jest: true,
      },
    },
  ],
};
