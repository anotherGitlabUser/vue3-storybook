module.exports = {
  stories: [
    '../../src/**/__stories__/*.stor(y|ies).@(js|jsx|ts|tsx|mdx)',
  ],
  addons: [
    '@storybook/addon-essentials',
     '@storybook/addon-links',
     'storybook-addon-extended-info',
    ]
}
