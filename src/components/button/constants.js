/**
 * @summary map types for button to html elements
 *
 * @description sometimes its useful a link has the same style like a button
 * so we can show an html-link with the same style as an button
 *
 * @type {Readonly<{button: string, link: string}>}
 */
const BUTTON_TYPES = Object.freeze({
  button: 'button',
  link: 'a',
});

/**
 * @summary map alignments for button to tailwind classes
 *
 * @type {Readonly<{left: string, center: string, right: string}>}
 */
const BUTTON_ALIGN = Object.freeze({
  center: 'tw-justify-center',
  left: 'tw-justify-start',
  right: 'tw-justify-end',
});

export {
  BUTTON_ALIGN,
  BUTTON_TYPES,
};
