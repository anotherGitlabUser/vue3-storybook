import ButtonDefault from '@/components/button/ButtonDefault.vue';

export default {
  title: 'Test/Button',
};

export const WithText = () => ({
  components: { ButtonDefault },
  template: '<button-default @click="action">Hello Button</button-default>',
});

export const WithSomeEmoji = () => ({
  components: { ButtonDefault },
  template: '<button-default>😀 😎 👍 💯</button-default>',
});
