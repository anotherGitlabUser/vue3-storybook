import {
  render,
  screen,
} from '@testing-library/vue';

import '@testing-library/jest-dom';

import {
  ButtonDefault,
} from '@/components/button';

const slots = {
  default: '<div data-testid="defaultSlot">slotContent</div>',
};

describe('ButtonDefault', () => {
  it('should render slot', () => {
    render(ButtonDefault, {
      slots,
    });

    expect(screen.getByTestId('defaultSlot'))
      .toBeVisible();
  });

  describe('type', () => {
    it('should render button by default', () => {
      render(ButtonDefault, {
        slots,
      });

      const elementToTest = screen.getByRole('button');

      expect(elementToTest)
        .toBeVisible();
    });

    it('should render button explicit', () => {
      render(ButtonDefault, {
        props: {
          type: 'button',
        },
        slots,
      });

      const elementToTest = screen.getByRole('button');

      expect(elementToTest)
        .toBeVisible();
    });

    it('should render link explicit', () => {
      render(ButtonDefault, {
        props: {
          href: 'foobar',
          type: 'link',
        },
        slots,
      });

      const elementToTest = screen.getByRole('link');

      expect(elementToTest)
        .toBeVisible();
    });

    it('should render link without attr `target` for internal links', () => {
      render(ButtonDefault, {
        props: {
          href: 'foobar',
          type: 'link',
        },
        slots,
      });

      const elementToTest = screen.getByRole('link');

      expect(elementToTest)
        .toBeVisible();
      expect(elementToTest)
        .not.toHaveAttribute('target');
    });

    it('should render link with attr `target=_blank` for external https links', () => {
      render(ButtonDefault, {
        props: {
          href: 'https://foobar',
          type: 'link',
        },
        slots,
      });

      const elementToTest = screen.getByRole('link');

      expect(elementToTest)
        .toBeVisible();
      expect(elementToTest)
        .toHaveAttribute('target', '_blank');
    });
  });

  describe('alt', () => {
    it('should render link without attr `alt` for link', () => {
      render(ButtonDefault, {
        props: {
          href: 'https://foobar',
          type: 'link',
        },
        slots,
      });

      const elementToTest = screen.getByRole('link');

      expect(elementToTest)
        .toBeVisible();
      expect(elementToTest)
        .not.toHaveAttribute('alt', '');
    });

    it('should render link attr `alt` for link', () => {
      const altText = 'altText';

      render(ButtonDefault, {
        props: {
          href: 'https://foobar',
          type: 'link',
          alt: altText,
        },
        slots,
      });

      const elementToTest = screen.getByRole('link');

      expect(elementToTest)
        .toBeVisible();
      expect(elementToTest)
        .toHaveAttribute('alt', altText);
    });
  });

  describe('cssClasses', () => {
    it('should render given css classes', () => {
      render(ButtonDefault, {
        props: {
          cssClasses: ['foo', 'bar'],
        },
        slots,
      });

      expect(screen.getByTestId('ButtonDefault'))
        .toHaveClass('foo', 'bar');
    });
  });

  describe('border', () => {
    it('should render borderless by default', () => {
      render(ButtonDefault, {
        slots,
      });

      expect(screen.getByTestId('ButtonDefault'))
        .not.toHaveClass('tw-border');
    });

    it('should render borderless explicit', () => {
      render(ButtonDefault, {
        props: {
          border: false,
        },
        slots,
      });

      expect(screen.getByTestId('ButtonDefault'))
        .not.toHaveClass('tw-border');
    });

    it('should render border explicit', () => {
      render(ButtonDefault, {
        props: {
          border: true,
        },
        slots,
      });

      expect(screen.getByTestId('ButtonDefault'))
        .toHaveClass('tw-border', 'tw-border-hb-blue-primary');
    });

    it('should render border explicit for link', () => {
      render(ButtonDefault, {
        props: {
          border: true,
          href: 'somelink',
          type: 'link',
        },
        slots,
      });

      expect(screen.getByTestId('ButtonDefault'))
        .toHaveClass('tw-border', 'tw-border-hb-blue-primary');
    });
  });

  describe('align', () => {
    it('should render slotcontent on center by default', () => {
      render(ButtonDefault, {
        slots,
      });

      expect(screen.getByTestId('ButtonDefault'))
        .toHaveClass('tw-justify-center');
    });

    it('should render slotcontent on center explicit', () => {
      render(ButtonDefault, {
        props: {
          align: 'center',
        },
        slots,
      });

      expect(screen.getByTestId('ButtonDefault'))
        .toHaveClass('tw-justify-center');
    });

    it('should render slotcontent on left explicit', () => {
      render(ButtonDefault, {
        props: {
          align: 'left',
        },
        slots,
      });

      expect(screen.getByTestId('ButtonDefault'))
        .toHaveClass('tw-justify-start');
    });

    it('should render slotcontent on right explicit', () => {
      render(ButtonDefault, {
        props: {
          align: 'right',
        },
        slots,
      });

      expect(screen.getByTestId('ButtonDefault'))
        .toHaveClass('tw-justify-end');
    });
  });

  describe('uppercase', () => {
    it('should render slotcontent not uppercase by default', () => {
      render(ButtonDefault, {
        slots,
      });

      expect(screen.getByTestId('ButtonDefault--Inner'))
        .not.toHaveClass('tw-uppercase');
    });

    it('should render slotcontent not uppercase explicit', () => {
      render(ButtonDefault, {
        props: {
          uppercase: false,
        },
        slots,
      });

      expect(screen.getByTestId('ButtonDefault--Inner'))
        .not.toHaveClass('tw-uppercase');
    });

    it('should render slotcontent uppercase explicit', () => {
      render(ButtonDefault, {
        props: {
          uppercase: true,
        },
        slots,
      });

      expect(screen.getByTestId('ButtonDefault--Inner'))
        .toHaveClass('tw-uppercase');
    });
  });
});
